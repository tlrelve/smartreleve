// MEAN Stack RESTful API Tutorial - Contact List App

var express = require('express');
var app = express();
var mongojs = require('mongojs');
var db = mongojs('tele', ['user','counter','comsuption','contract','contactlist']);
var bodyParser = require('body-parser');
var connexions = require('./connection');

var Consom = require('./db').Consom;
app.use(express.static(__dirname + '/public'));
app.use('/bower_components',express.static(__dirname + '/bower_components'));

app.use(bodyParser.json());
app.use('/connection', connexions);

app.get('/user', function (req, res) {
  console.log('I received a GET request');

  db.user.find(function (err, docs) {
    console.log(docs);
    res.json(docs);
  });
});

app.get('/user/:mail/:password', function(req, res){
  var email = req.params.mail;
  var pswd = req.params.password;
  db.user.find({mail: email, password: pswd}, function(err, doc){
    res.json(doc);
  });
});

app.post('/user', function (req, res) {
  console.log(req.body);
  db.user.insert(req.body, function(err, doc) {
    res.json(doc);
  });
});

app.delete('/user/:id', function (req, res) {
  var id = req.params.id;
  console.log(id);
  db.user.remove({_id: mongojs.ObjectId(id)}, function (err, doc) {
    res.json(doc);
  });
});

app.get('/user/:id', function (req, res) {
  var id = req.params.id;
  console.log(id);
  db.user.findOne({_id: mongojs.ObjectId(id)}, function (err, doc) {
    res.json(doc);
  });
});

app.put('/user/:id', function (req, res) {
  var id = req.params.id;
  console.log(req.body.name);
  db.user.findAndModify({
    query: {_id: mongojs.ObjectId(id)},
    update: {$set: {name: req.body.name, lastname: req.body.lastname, login: req.body.login, password: req.body.password, mail: req.body.mail, adresse: req.body.adresse, potalCode: req.body.potalCode, city: req.body.city, country: req.body.country}},
    new: true}, function (err, doc) {
      res.json(doc);
    }
  );
});

app.get('/counter', function (req, res) {
  console.log('I received a GET request');

  db.counter.find(function (err, docs) {
    console.log(docs);
    res.json(docs);
  });
});

app.post('/counter', function (req, res) {
  console.log(req.body);
  db.counter.insert(req.body, function(err, doc) {
    res.json(doc);
  });
});

app.delete('/counter/:id', function (req, res) {
  var id = req.params.id;
  console.log(id);
  db.counter.remove({_id: mongojs.ObjectId(id)}, function (err, doc) {
    res.json(doc);
  });
});

app.get('/counter/:id', function (req, res) {
  var id = req.params.id;
  console.log(id);
  db.counter.findOne({_id: mongojs.ObjectId(id)}, function (err, doc) {
    res.json(doc);
  });
});

app.put('/counter/:id', function (req, res) {
  var id = req.params.id;
  console.log(req.body.name);
  db.counter.findAndModify({
    query: {_id: mongojs.ObjectId(id)},
    update: {$set: {name: req.body.name, type: req.body.type, registretionDate: req.body.registretionDate, idUser: req.body.idUser}},
    new: true}, function (err, doc) {
      res.json(doc);
    }
  );
});

app.get('/comsuption', function (req, res) {
    Consom.find({}, function (err, consoms) {
        if (err) return console.error(err);
        res.json(consoms);
    });
});

app.post('/comsuption', function (req, res) {
  console.log(req.body);
  db.comsuption.insert(req.body, function(err, doc) {
    res.json(doc);
  });
});

app.delete('/comsuption/:id', function (req, res) {
  var id = req.params.id;
  console.log(id);
  db.comsuption.remove({_id: mongojs.ObjectId(id)}, function (err, doc) {
    res.json(doc);
  });
});

app.get('/comsuption/:id', function (req, res) {
  var id = req.params.id;
  console.log(id);
  db.comsuption.findOne({_id: mongojs.ObjectId(id)}, function (err, doc) {
    res.json(doc);
  });
});

app.put('/comsuption/:id', function (req, res) {
  var id = req.params.id;
  console.log(req.body.name);
  db.comsuption.findAndModify({
    query: {_id: mongojs.ObjectId(id)},
    update: {$set: {idCounter: req.body.idCounter, value: req.body.value, status: req.body.status, valueDate: req.body.valueDate}},
    new: true}, function (err, doc) {
      res.json(doc);
    }
  );
});

app.get('/contract', function (req, res) {
  console.log('I received a GET request');

  db.contract.find(function (err, docs) {
    console.log(docs);
    res.json(docs);
  });
});

app.post('/contract', function (req, res) {
  console.log(req.body);
  db.contract.insert(req.body, function(err, doc) {
    res.json(doc);
  });
});

app.delete('/contract/:id', function (req, res) {
  var id = req.params.id;
  console.log(id);
  db.contract.remove({_id: mongojs.ObjectId(id)}, function (err, doc) {
    res.json(doc);
  });
});

app.get('/contract/:id', function (req, res) {
  var id = req.params.id;
  console.log(id);
  db.contract.findOne({_id: mongojs.ObjectId(id)}, function (err, doc) {
    res.json(doc);
  });
});

app.put('/contract/:id', function (req, res) {
  var id = req.params.id;
  console.log(req.body.name);
  db.contract.findAndModify({
    query: {_id: mongojs.ObjectId(id)},
    update: {$set: {name: req.body.name, idUser: req.body.idUser, dateDebu: req.body.dateDebu, dateFin: req.body.dateFin}},
    new: true}, function (err, doc) {
      res.json(doc);
    }
  );
});

app.get('/contactlist', function (req, res) {
  console.log('I received a GET request');

  db.contactlist.find(function (err, docs) {
    console.log(docs);
    res.json(docs);
  });
});

app.post('/contactlist', function (req, res) {
  console.log(req.body);
  db.contactlist.insert(req.body, function(err, doc) {
    res.json(doc);
  });
});

app.delete('/contactlist/:id', function (req, res) {
  var id = req.params.id;
  console.log(id);
  db.contactlist.remove({_id: mongojs.ObjectId(id)}, function (err, doc) {
    res.json(doc);
  });
});

app.get('/contactlist/:id', function (req, res) {
  var id = req.params.id;
  console.log(id);
  db.contactlist.findOne({_id: mongojs.ObjectId(id)}, function (err, doc) {
    res.json(doc);
  });
});

app.put('/contactlist/:id', function (req, res) {
  var id = req.params.id;
  console.log(req.body.name);
  db.contactlist.findAndModify({
    query: {_id: mongojs.ObjectId(id)},
    update: {$set: {name: req.body.name, email: req.body.email, number: req.body.number}},
    new: true}, function (err, doc) {
      res.json(doc);
    }
  );
});

app.listen(3000);
console.log("Server running on port 3000");
