/**
 * @author claireDupont
 */
 (function () {
  'use strict';

  angular.module('SmartReleve.login', [])
  .controller('indexCtrl', indexCtrl);

  /** @ngInject */
  function indexCtrl($scope, $http) {
    // Gestion de la session
    $scope.isSession = function(){
        return window.sessionStorage.length>0;
    }

    function redirectIfLogged(){
      if ($scope.isSession()){
        window.location.assign('/app.html');
      }
    }

    redirectIfLogged();

    $scope.addSessionItem = function(id, nom, prenom, mail){
        window.sessionStorage.setItem('id', id);
        window.sessionStorage.setItem('nom', nom);
        window.sessionStorage.setItem('prenom', prenom);
        window.sessionStorage.setItem('mail', mail);
    }

    //Redirection
    document.getElementById("errorLog").style.display = 'none';
    $scope.connection = function(){
      var login = $scope.login;
      var pswd = $scope.pswd;
      $http.get('/user/'+login+'/'+pswd).then(function onSuccess(response){
        if(response.data.length == 0){
          document.getElementById("errorLog").style.display = 'block';
        } else {
          $scope.addSessionItem(response.data[0]._id, response.data[0].name, response.data[0].lastname, response.data[0].mail);
          window.location.assign('/app.html');
        }
      },
      function onError(error){});
    }
  }

})();
