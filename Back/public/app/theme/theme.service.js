/**
 * Created by k.danovsky on 12.05.2016.
 */

(function () {
  'use strict';

  angular.module('SmartReleve.theme')
    .service('themeLayoutSettings', themeLayoutSettings);

  /** @ngInject */
  function themeLayoutSettings(baConfig) {
    var isMobile = (/android|webos|iphone|ipad|ipod|blackberry|windows phone/).test(navigator.userAgent.toLowerCase());
    var mobileClass = isMobile ? 'mobile' : '';
    var smartClass = baConfig.theme.smart ? 'smart-theme' : '';
    angular.element(document.body).addClass(mobileClass).addClass(smartClass);

    return {
      smart: baConfig.theme.smart,
      mobile: isMobile,
    }
  }

})();