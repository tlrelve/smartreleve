/**
 * @author v.lugovsky
 * created on 15.12.2015
 */
(function () {
  'use strict';

  angular.module('SmartReleve.theme', [
      'toastr',
      'chart.js',
      'angular-chartist',
      'angular.morris-chart',
      'textAngular',
      'SmartReleve.theme.components',
      'SmartReleve.theme.inputs'
  ]);

})();
