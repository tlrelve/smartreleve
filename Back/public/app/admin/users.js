/**
 * @author claireDupont
 */
 (function () {
  'use strict';

  angular.module('SmartReleve',[])
  .controller('users', MonCompteCtrl);

  /** @ngInject */
  function MonCompteCtrl($scope, $http, $filter, editableOptions, editableThemes, $uibModal) {

    var currentUserId = window.sessionStorage.getItem('id');
    var refresh = function() {
      $http.get('/user/'+ currentUserId).success(function(response) {
          $scope.formData = response;
        });

        //Récupération des compteurs
        $scope.compteurs = [];
        $http.get('/counter').success(function(response){
          $scope.compteurList = response;
        }).then(function(){
          angular.forEach($scope.compteurList, function(value, key){
            $scope.compteurs.push({
              id: value._id,
              numero: value.numeroCompteur,
              nom: value.nom,
              modele: value.modele,
              type: value.type,
              date: value.registretionDate
            });
          });
        });
      };

      refresh();

      $scope.addCompteur = function() {
        $scope.inserted = {
          id: '',
          nom: '',
          numero: '',
          modele: '',
          type: '',
          date: ''
        };
        $scope.compteurs.push($scope.inserted);
      };

  // Création ou modification d'un compteur
      $scope.updateCompteur = function(compteur){
        compteur.mail = window.sessionStorage.getItem('mail');
        if(compteur.id == ''){
          $http.put('counter/new/' + compteur.mail, compteur).success(function(response){
            refresh();
          });
        } else {
          $http.put('counter/' + compteur.id, compteur).success(function(response){
            refresh();
          });
        }
        return true;
      };

      // On annule la création d'une ligne
      $scope.cancel = function(index, id){
        if (id == ''){
          $scope.compteurs.splice(index, 1);
        }else {
          refresh();
        }
      }

      var test;
      // Suppression dun capteur
      $scope.showConfirm = function(ev, compteur, index){
        var modalInstance = $uibModal.open({
         animation: true,
         templateUrl: 'myModalContent.html',
         controller: 'ModalInstanceCtrl',
         size: 'sm',
         scope: $scope,
         resolve: {
           value: compteur,
           idx: index
         }
       });

       modalInstance.result.then(function (result) {
         $scope.removeCompteur(result.compteur, result.index);
       }, function () {});
      }

      $scope.removeCompteur = function(compteur, index) {
        $scope.compteurs.splice(index, 1);
        $http.delete('counter/' + compteur.id).success(function(response){
          refresh();
        });
      };

    }
  })();

  angular.module('SmartReleve.pages.monCompte').controller('ModalInstanceCtrl', function ($scope, $uibModalInstance, value, idx) {
    $scope.item = value;
    $scope.ok = function () {
      $uibModalInstance.close({compteur: value, index: idx});
    };

    $scope.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };
  });
