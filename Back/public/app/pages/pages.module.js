/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('SmartReleve.pages', [
    'ui.router',

    'SmartReleve.pages.monCompte',
    'SmartReleve.pages.tableauDeBord',
    'SmartReleve.pages.vueGlobale',
  ])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($urlRouterProvider, baSidebarServiceProvider) {
    $urlRouterProvider.otherwise('/monCompte');
  }
})();
