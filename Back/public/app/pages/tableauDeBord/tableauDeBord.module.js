/**
 * @author sabahAMRAOUI
 */
(function () {
  'use strict';

  angular.module('SmartReleve.pages.tableauDeBord', [])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
        .state('tableauDeBord', {
          url: '/tableauDeBord',
          templateUrl: 'app/pages/tableauDeBord/tableauDeBord.html',
          title: 'Tableau de bord',
          controller: 'TableauDeBordCtrl',
          sidebarMeta: {
            icon: 'ion-speedometer',
            order: 1,
          },
        });
  }

})();
