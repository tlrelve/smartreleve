-/**
 * @author claireDupont
 */
 (function () {
  'use strict';

  angular.module('SmartReleve.pages.tableauDeBord')
  .controller('TableauDeBordCtrl', TableauDeBordCtrl);

  /** @ngInject */
  function TableauDeBordCtrl($scope, $http, $filter, editableOptions, editableThemes, $uibModal, baConfig, layoutPaths) {
        var userObj;
        var consomations;
        var compteurs;
        var currentUserId;
        $scope.numCompteur = "";
        $scope.counterlist = [];

        var refresh = function() {
          currentUserId = window.sessionStorage.getItem('id');
          compteurs = [];
          $http.get('/user/'+ currentUserId).success(function(response) {
              userObj = response;
              getCounters();
          });
        };

        refresh();

        $scope.refreshChart = function() {
          console.log("enter refresh");
            console.log($scope.numCompteur);
          console.log(document.getElementById('selectCounter').value);
          $scope.numCompteur = document.getElementById('selectCounter').value;
          refresh();
        };

        var getCounters = function() {
          $http.get('/userCounter/'+userObj.mail).success(function(response){
              for(var i=0;i<response.length;i++){
                compteurs.push(response[i].numeroCompteur);
              }
              $scope.counterlist = compteurs;
              if($scope.numCompteur ==""){
                $scope.numCompteur = (compteurs.length > 0)? compteurs[0] : "";
              }
              getConsumption();
            });
        };

        var getConsumption = function() {
          var datesAndValues = [];

          $http.get('/userConsommation/'+$scope.numCompteur).success(function(response){
            //response = response.valueDate[].sort();
            if(response.length >0){
                for(var j=0;j<response.length;j++){
                  var DateFormatValue = new Date(parseInt(response[j].valueDate,10));
                  DateFormatValue = $filter('date')(DateFormatValue, 'dd/MM/yyyy');
                  datesAndValues.push({value: response[j].value,valueC: response[j].value, date: DateFormatValue.toString()});
                }
                //initChart(datesAndValues,$scope.numCompteur);
                datesAndValues = datesAndValues.sort();
                initChart(datesAndValues,$scope.numCompteur);
                initChartMoy(datesAndValues,$scope.numCompteur);

            } else {
              var myEl = angular.element( document.querySelector( '#lineChart' ) );
              myEl.empty();
              myEl.html("Aucune donnée à afficher");
              var chartConsoMoy = angular.element( document.querySelector( '#zoomAxisChart' ) );
              chartConsoMoy.empty();
              chartConsoMoy.html("Aucune donnée à afficher");
            }
          });
        };

        var initChartMoy = function(datesAndValues,compteur) {
          var values;
          var currentYear;
          var lastYear;
          var consoN = [];
          var consoN_1 = [];
          var consoMoyenneN = [];
          var consoMoyenneN_1 = [];
          var consoMoyenne = [];


          currentYear= new Date().getFullYear();
          lastYear= currentYear - 1;

          for(var i=0;i<datesAndValues.length;i++){
            if(currentYear == datesAndValues[i].date.split('/')[2]){
              consoN.push({value: datesAndValues[i].value, date: datesAndValues[i].date});
            } else if(lastYear == datesAndValues[i].date.split('/')[2]){
              consoN_1.push({value: datesAndValues[i].value, date: datesAndValues[i].date});
            }
          }

          for(var j=1;j<=12;j++){
            var S = 0;
            var n = 0;
            for(var i=0;i<consoN.length;i++){
                if(j == parseInt(consoN[i].date.split('/')[1])){
                  S+= consoN[i].value;
                  n++;
                }
            }
            if(n>0){
              consoMoyenneN.push({date: j, value: S/n});
            } else {
              consoMoyenneN.push({date: j, value: 0});
            }
          }
          for(var j=1;j<=12;j++){
            var S = 0;
            var n = 0;
            for(var i=0;i<consoN_1.length;i++){
                if(j == parseInt(consoN_1[i].date.split('/')[1])){
                  S+= consoN_1[i].value;
                  n++;
                }
            }
            if(n>0){
              consoMoyenneN_1.push({date: j, value: S/n});
            } else {
              consoMoyenneN_1.push({date: j, value: 0});
            }
          }

          for(var j=0;j<12;j++){
            if(j<9) consoMoyenne.push({date: '15/0'+(j+1)+'/'+currentYear, consoG: consoMoyenneN[j].value, consoP2: consoMoyenneN_1[j].value});
            else consoMoyenne.push({date: '15/'+(j+1)+'/'+currentYear, consoG: consoMoyenneN[j].value, consoP2: consoMoyenneN_1[j].value});
          }

          var layoutColors = baConfig.colors;
          var id = "zoomAxisChart";
          var chart = AmCharts.makeChart(id, {
            "type": "serial",
            "theme": "none",
            "color": layoutColors.defaultText,
            "dataDateFormat": "DD/MM/YYYY",
            "precision": 2,
            "valueAxes": [{
              color: layoutColors.defaultText,
              axisColor: layoutColors.defaultText,
              gridColor: layoutColors.defaultText,
              "id": "v1",
              "title": "Consommation",
              "position": "left",
              "autoGridCount": false,
              "labelFunction": function(value) {
                return "" + Math.round(value) + "L";
              }
            }],
            "graphs": [{
              "id": "g3",
              color: layoutColors.defaultText,
              "valueAxis": "v1",
              "lineColor": layoutColors.primaryLight,
              "fillColors": layoutColors.primaryLight,
              "fillAlphas": 0.8,
              "lineAlpha": 0.8,
              "type": "column",
              "title": "Consommation personnelle moyenne année N-1",
              "valueField": "consoP2",
              "clustered": false,
              "columnWidth": 15,
              "lineColorField" : layoutColors.defaultText,
              "legendValueText": "[[value]]L",
              "balloonText": "[[title]]<br/><b style='font-size: 130%'>[[value]]L</b>"
            }, {
              "id": "g4",
              "valueAxis": "v1",
              color: layoutColors.defaultText,
              "lineColor": layoutColors.primary,
              "fillColors": layoutColors.primary,
              "fillAlphas": 0.9,
              "lineAlpha": 0.9,
              "type": "column",
              "title": "Consommation personnelle moyenne année N",
              "valueField": "consoG",
              "clustered": false,
              "columnWidth": 10,
              "legendValueText": "[[value]]L",
              "balloonText": "[[title]]<br/><b style='font-size: 130%'>[[value]]L</b>"
            }],
            "chartScrollbar": {
              "graph": "g1",
              "oppositeAxis": false,
              "offset": 30,
              gridAlpha: 0,
              color: layoutColors.defaultText,
              scrollbarHeight: 50,
              backgroundAlpha: 0,
              selectedBackgroundAlpha: 0.05,
              selectedBackgroundColor: layoutColors.defaultText,
              graphFillAlpha: 0,
              autoGridCount: true,
              selectedGraphFillAlpha: 0,
              graphLineAlpha: 0.2,
              selectedGraphLineColor: layoutColors.defaultText,
              selectedGraphLineAlpha: 1
            },
            "chartCursor": {
              "pan": true,
              "cursorColor" : layoutColors.danger,
              "valueLineEnabled": true,
              "valueLineBalloonEnabled": true,
              "cursorAlpha": 0,
              "valueLineAlpha": 0.2
            },
            "categoryField": "date",
            "categoryAxis": {
              "axisColor": layoutColors.defaultText,
              "color": layoutColors.defaultText,
              "gridColor": layoutColors.defaultText,
              "parseDates": true,
              "dashLength": 1,
              "minorGridEnabled": true
            },
            "legend": {
              "useGraphSettings": true,
              "position": "top",
              "color": layoutColors.defaultText
            },
            "balloon": {
              "borderThickness": 1,
              "shadowAlpha": 0
            },
            "export": {
              "enabled": true
            },
            "dataProvider": consoMoyenne,
            pathToImages: layoutPaths.images.amChart
          });
        };

        var initChart = function(datesAndValues,compteur) {


                  if (datesAndValues!=null&&datesAndValues.length>0){
                      for(var i=1;i<datesAndValues.length;i++){
                        var Garder = datesAndValues[i].value;
                        datesAndValues[i].value = datesAndValues[i].value - datesAndValues[i-1].valueC;
                      }
                  }

                  var layoutColors = baConfig.colors;
                  //var id = $element[0].getAttribute('id');
                  var id = 'lineChart';

                  var vueDetailleeChart = AmCharts.makeChart(id, {
                    type: 'serial',
                    theme: 'smart',
                    color: layoutColors.defaultText,
                    marginTop: 0,
                    marginRight: 15,
                    dataProvider: datesAndValues,
                    valueAxes: [
                      {
                        axisAlpha: 0,
                        position: 'left',
                        gridAlpha: 0.5,
                        gridColor: layoutColors.border,
                      }
                    ],
                    graphs: [
                      {
                        id: 'g1',
                        balloonText: '[[value]]',
                        bullet: 'round',
                        bulletSize: 8,
                        lineColor: layoutColors.danger,
                        lineThickness: 1,
                        negativeLineColor: layoutColors.warning,
                        type: 'smoothedLine',
                        valueField: 'value'
                      }
                    ],
                    chartScrollbar: {
                      graph: 'g1',
                      gridAlpha: 0,
                      color: layoutColors.defaultText,
                      scrollbarHeight: 55,
                      backgroundAlpha: 0,
                      selectedBackgroundAlpha: 0.05,
                      selectedBackgroundColor: layoutColors.defaultText,
                      graphFillAlpha: 0,
                      autoGridCount: true,
                      selectedGraphFillAlpha: 0,
                      graphLineAlpha: 0.2,
                      selectedGraphLineColor: layoutColors.defaultText,
                      selectedGraphLineAlpha: 1
                    },
                    chartCursor: {
                      categoryBalloonDateFormat: 'DD/MM/YYYY',
                      cursorAlpha: 0,
                      valueLineEnabled: true,
                      valueLineBalloonEnabled: true,
                      valueLineAlpha: 0.5,
                      fullWidth: true
                    },
                    dataDateFormat: 'DD/MM/YYYY',
                    categoryField: 'date',
                    categoryAxis: {
                      minPeriod: 'DD/MM/YYYY',
                      parseDates: false,
                      minorGridAlpha: 0.1,
                      minorGridEnabled: true,
                      gridAlpha: 0.5,
                      gridColor: layoutColors.border,
                    },
                    export: {
                      enabled: true
                    },
                    creditsPosition: 'bottom/right',
                    pathToImages: layoutPaths.images.amChart
                  });

                  vueDetailleeChart.addListener('rendered', zoomChart);
                  if (vueDetailleeChart.zoomChart) {
                    vueDetailleeChart.zoomChart();
                  }
              };

        function zoomChart() {
          vueDetailleeChart.zoomToIndexes(Math.round(vueDetailleeChart.dataProvider.length * 0.4), Math.round(vueDetailleeChart.dataProvider.length * 0.55));
        }

        //Récupération des compteurs
        $scope.compteurs = [];
        $http.get('/counter/mail/'+ window.sessionStorage.getItem('mail')).success(function(response){
          $scope.compteurList = response;
        }).then(function(){
          angular.forEach($scope.compteurList, function(value, key){
            $scope.compteurs.push({
              id: value._id,
              numero: value.numeroCompteur,
              nom: value.nom,
              modele: value.modele,
              type: value.type,
              date: value.registretionDate
            });
          });
        });


        $scope.selectCounter =  function(num){
          $scope.numCompteur = num;
          console.log("here");
          console.log($scope.numCompteur);
          refresh();
        }
      };

  })();
