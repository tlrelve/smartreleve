/**
 * @author claireDupont
 */
 (function () {
  'use strict';

  angular.module('SmartReleve.pages.monCompte')
  .controller('MonCompteCtrl', MonCompteCtrl);

  /** @ngInject */
  function MonCompteCtrl($scope, $http, $filter, editableOptions, editableThemes, $uibModal) {

    var currentUserId = window.sessionStorage.getItem('id');
    var refresh = function() {
      $http.get('/user/'+ currentUserId).success(function(response) {
          $scope.formData = response;
        });

        //Récupération des compteurs
        $scope.compteurs = [];
        $http.get('/counter/mail/'+ window.sessionStorage.getItem('mail')).success(function(response){
          $scope.compteurList = response;
        }).then(function(){
          angular.forEach($scope.compteurList, function(value, key){
            $scope.compteurs.push({
              id: value._id,
              numero: value.numeroCompteur,
              nom: value.nom,
              modele: value.modele,
              type: value.type,
              date: value.registretionDate
            });
          });
        });
      };

      refresh();

      $scope.errorChamps = false;
      $scope.errorPass = false;
      $scope.errorPassDif = false;
      var changePass = false;

      $scope.update = function() {
        $scope.user = {};
        if(document.getElementById("autocomplete") != null){
        $scope.formData.address = document.getElementById("autocomplete").value;
        }

        if(document.getElementById("inputVille") != null){
        $scope.formData.city = document.getElementById("inputVille").value;
        }

        if(document.getElementById("inputCP") != null){
        $scope.formData.postalcode = document.getElementById("inputCP").value;
        }

        if(document.getElementById("inputPays") != null){
        $scope.formData.country = document.getElementById("inputPays").value;
        }
        if($scope.formData && $scope.formData.name && $scope.formData.name !== ""
                       && $scope.formData.lastname && $scope.formData.lastname !== ""
                       && $scope.formData.mail && $scope.formData.mail !== ""
                       && $scope.formData.address && $scope.formData.address !== ""
                       && $scope.formData.postalcode && $scope.formData.postalcode !== ""
                       && $scope.formData.city && $scope.formData.city !== ""
                       && $scope.formData.country && $scope.formData.country !== ""){
          $scope.errorChamps = false;
          if($scope.formData.passwordNew && $scope.formData.passwordNew !== ""){
            changePass = true;
            if ($scope.formData.passwordConfirmation && $scope.formData.passwordConfirmation !== ""){
              $scope.errorPass = false;
              if($scope.formData.passwordConfirmation === $scope.formData.passwordNew){
                $scope.errorPassDif = false;
                changePass = false;
                $scope.user.password = $scope.formData.passwordNew;
              }
              else{
                $scope.errorPassDif = true;
              }
            }
            else{
              $scope.errorPass = true;
            }
          }
          else {
            changePass = false;
          }

          $scope.user.name = $scope.formData.name;
          $scope.user.lastname = $scope.formData.lastname;
          $scope.user.mail = $scope.formData.mail;
          $scope.user.address = $scope.formData.address;
          $scope.user.postalcode = $scope.formData.postalcode;
          $scope.user.city = $scope.formData.city;
          $scope.user.country = $scope.formData.country;

          if (!changePass){
            window.sessionStorage.setItem('mail', $scope.user.mail);
            $http.put('/user/' + currentUserId, $scope.user).success(function(response) {
              alert("Votre profil a bien été modifié");
              refresh();
            })
          }
        }
        else{
          $scope.errorChamps = true;
        }
      };

      $scope.addCompteur = function() {

           $http.get('/counterlist').success(function(response){
             $scope.inserted = {
               id: '',
               nom: '',
               numero: response[0]+1,
               modele: '',
               type: '',
               date: +new Date
             };
             $scope.compteurs.push($scope.inserted);
           });
      };



  // Création ou modification d'un compteur
      $scope.updateCompteur = function(compteur){
        compteur.mail = window.sessionStorage.getItem('mail');
        if(compteur.id == ''){
          $http.put('counter/new/' + compteur.mail, compteur).success(function(response){
            refresh();
          });
        } else {
          $http.put('counter/' + compteur.id, compteur).success(function(response){
            refresh();
          });
        }
        return true;
      };

      // On annule la création d'une ligne
      $scope.cancel = function(index, id){
        if (id == ''){
          $scope.compteurs.splice(index, 1);
        }else {
          refresh();
        }
      }

      var test;
      // Suppression dun capteur
      $scope.showConfirm = function(ev, compteur, index){
        var modalInstance = $uibModal.open({
         animation: true,
         templateUrl: 'myModalContent.html',
         controller: 'ModalInstanceCtrl',
         size: 'sm',
         scope: $scope,
         resolve: {
           value: compteur,
           idx: index
         }
       });

       modalInstance.result.then(function (result) {
         $scope.removeCompteur(result.compteur, result.index);
       }, function () {});
      }

      $scope.removeCompteur = function(compteur, index) {
        $scope.compteurs.splice(index, 1);
        $http.delete('counter/' + compteur.id).success(function(response){
          refresh();
        });
      };

    }
  })();

  angular.module('SmartReleve.pages.monCompte').controller('ModalInstanceCtrl', function ($scope, $uibModalInstance, value, idx) {
    $scope.item = value;
    $scope.ok = function () {
      $uibModalInstance.close({compteur: value, index: idx});
    };

    $scope.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };
  });
