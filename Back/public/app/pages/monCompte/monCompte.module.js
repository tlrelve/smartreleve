/**
 * @author claireDupont
 */
(function () {
  'use strict';

  angular.module('SmartReleve.pages.monCompte', [])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
        .state('monCompte', {
          url: '/monCompte',
          templateUrl: 'app/pages/monCompte/monCompte.html',
          title: 'Mon Compte',
          controller: 'MonCompteCtrl',
          sidebarMeta: {
            icon: 'ion-person',
            order: 0,
          },
        });
  }

})();
