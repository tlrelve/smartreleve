/**
 * @author claireDupont
 */
 (function () {
  'use strict';

  angular.module('SmartReleve.pages.monCompte')
  .controller('VueGlobaleCtrl', VueGlobaleCtrl);

  /** @ngInject */
  function VueGlobaleCtrl($scope, $http, $filter) {
    $scope.exportData = function () {
        var blob = new Blob([document.getElementById('exportable').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, "vueGlobale.xls");
    };


    function sortByDate(item1, item2){
      return item1.valueDate - item2.valueDate;
    }

    //Récupération des compteurs
    $scope.compteurs = [];
    $scope.consommations = [];
    $http.get('/counter/mail/'+ window.sessionStorage.getItem('mail')).success(function(response){
      $scope.compteurList = response;
    }).then(function(){
      angular.forEach($scope.compteurList, function(value, key){
        $http.get('/userConsommation/'+value.numeroCompteur).success(function (consommationsCompteur) {
          var moyParJour = 0;
          var moyParMois = 0;
          var moyParAnnee = 0;
          if(consommationsCompteur.length !== 0){
            var consommationsCompteurTriee = consommationsCompteur.sort(sortByDate);
            var difference = consommationsCompteurTriee[consommationsCompteurTriee.length - 1].valueDate - consommationsCompteurTriee[0].valueDate;
            var nbJours = Math.floor(difference/1000/60/60/24);
            moyParJour = consommationsCompteurTriee[consommationsCompteurTriee.length - 1].value;
            if (nbJours !== 0){
              moyParJour = moyParJour / nbJours;
            }

            if (nbJours >= 30){
              var nbMois = new Date(consommationsCompteurTriee[consommationsCompteurTriee.length - 1].valueDate).getMonth()
                            - new Date(consommationsCompteurTriee[0].valueDate).getMonth();
              moyParMois = consommationsCompteurTriee[consommationsCompteurTriee.length - 1].value;
              if (nbMois !== 0){
                moyParMois = moyParMois / nbMois;
              }
            }

            if (nbJours >= 365){
              var nbAnnee = new Date(consommationsCompteurTriee[consommationsCompteurTriee.length - 1].valueDate).getFullYear()
                            - new Date(consommationsCompteurTriee[0].valueDate).getFullYear();
              moyParAnnee = consommationsCompteurTriee[consommationsCompteurTriee.length - 1].value;
              if (nbAnnee !== 0){
                moyParAnnee = moyParAnnee / nbAnnee;
              }
            }
          }

          $scope.consommations.push({
            date      : $filter('date')(new Date(), 'dd/MM/yyyy'),
            compteur  : value.nom,
            jour      : moyParJour.toFixed(2),
            mois      : moyParMois.toFixed(2),
            an        : moyParAnnee.toFixed(2)
          })
        })
      })
    });
  }
})();
