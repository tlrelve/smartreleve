/**
 * @author claireDupont
 */
(function () {
  'use strict';

  angular.module('SmartReleve.pages.vueGlobale', [])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
        .state('vueGlobale', {
          url: '/vueGlobale',
          templateUrl: 'app/pages/vueGlobale/vueGlobale.html',
          title: 'Vue Globale',
          controller: 'VueGlobaleCtrl',
          sidebarMeta: {
            icon: 'ion-earth',
            order: 3,
          },
        });
  }

})();
