'use strict';

angular.module('SmartReleve', [
  'ngAnimate',
  'ui.bootstrap',
  'ui.sortable',
  'ui.router',
  'ngTouch',
  'toastr',
  'smart-table',
  "xeditable",
  'ui.slimscroll',
  'ngJsTree',
  'angular-progress-button-styles',

  'SmartReleve.theme',
  'SmartReleve.pages',
  'SmartReleve.login',
  'SmartReleve.app',
]);
