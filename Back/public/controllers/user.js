var myApp = angular.module('SmartReleve', []);
myApp.controller('user', ['$scope', '$http', function($scope, $http) {
    console.log("Hello World from controller");


var refresh = function() {
  $http.get('/user').success(function(response) {
    console.log("I got the data I requested");
    $scope.userlist = response;
    $scope.user = "";
  });
};

refresh();

$scope.adduser = function() {
  console.log($scope.user);
  $http.post('/user', $scope.user).success(function(response) {
    console.log(response);
    window.location.assign('../');
  });
};


$scope.remove = function(id) {
  console.log(id);
  $http.delete('/user/' + id).success(function(response) {
    refresh();
  });
};

$scope.edit = function(mail) {
  console.log(mail);
  $http.get('/user/' + mail).success(function(response) {
    $scope.user = response;
  });
};

$scope.update = function() {
  console.log($scope.user._id);
  $http.put('/user/' + $scope.user._id, $scope.user).success(function(response) {
    refresh();
  })
};

$scope.deselect = function() {
  $scope.user = "";
}

}]);﻿
