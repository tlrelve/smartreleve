var myApp = angular.module('SmartReleve', []);
myApp.controller('compteur', ['$scope', '$http', function($scope, $http) {
    console.log("Hello World from controller");


var refresh = function() {
  $http.get('/counter').success(function(response) {
    console.log("I got the data I requested");
    $scope.counterlist = response;
    $scope.counter = "";
  });
};

refresh();

$scope.addcounter = function() {
  console.log($scope.counter);
  $http.post('/counter', $scope.counter).success(function(response) {
    console.log(response);
    refresh();
  });
};

$scope.remove = function(id) {
  console.log(id);
  $http.delete('/counter/' + id).success(function(response) {
    refresh();
  });
};

$scope.edit = function(id) {
  console.log(id);
  $http.get('/counter/' + id).success(function(response) {
    $scope.counter = response;
  });
};

$scope.update = function() {
  console.log($scope.counter._id);
  $http.put('/counter/' + $scope.counter._id, $scope.counter).success(function(response) {
    refresh();
  })
};

$scope.deselect = function() {
  $scope.counter = "";
}

}]);﻿
