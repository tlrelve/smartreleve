var myApp = angular.module('SmartReleve', []);
myApp.controller('AppCtrl', ['$scope', '$http', function($scope, $http) {
    console.log("Hello World from controller");


var refresh = function() {
  $http.get('/contract').success(function(response) {
    console.log("I got the data I requested");
    $scope.contractlist = response;
    $scope.contract = "";
  });
};

refresh();

$scope.addcontract = function() {
  console.log($scope.contract);
  $http.post('/contract', $scope.contract).success(function(response) {
    console.log(response);
    refresh();
  });
};

$scope.remove = function(id) {
  console.log(id);
  $http.delete('/contract/' + id).success(function(response) {
    refresh();
  });
};

$scope.edit = function(id) {
  console.log(id);
  $http.get('/contract/' + id).success(function(response) {
    $scope.contract = response;
  });
};

$scope.update = function() {
  console.log($scope.contract._id);
  $http.put('/contract/' + $scope.contract._id, $scope.contract).success(function(response) {
    refresh();
  })
};

$scope.deselect = function() {
  $scope.contract = "";
}

}]);﻿
