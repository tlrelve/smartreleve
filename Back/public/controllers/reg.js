var myApp = angular.module('SmartReleve', []);
myApp.controller('reg', ['$scope', '$http', function($scope, $http) {

	$scope.adduser = function() {
		$scope.message = "";

		document.getElementById("message").style.display = 'none';

	   
		verification();
	};
		   
  function bonmail(mailteste)
  {
	var reg = new RegExp('^[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*@[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*[\.]{1}[a-z]{2,6}$', 'i');

	if(reg.test(mailteste))
	{
		return(true);
	}
	else
	{
		return(false);
	}
}
	function verification(){
		var name=document.getElementById("name").value;
		var lastname=document.getElementById("lastname").value;
		var mail=document.getElementById("mail").value;
		var adresse=document.getElementById("adresse").value;
		var ville=document.getElementById("ville").value;
		var pays=document.getElementById("pays").value;
		var codePostal=document.getElementById("codePostal").value;
		var psw=document.getElementById("psw").value;


		var newUser = [];

		if(mail != ""){
			$http.get('/user/mail/'+ mail).success(function(response) {

			    newUser = response;

			}).then(function(){
				if(newUser.length == 0){
					if(name==""){
						$scope.message = "Merci de renseigner votre prénom!";
						document.getElementById("message").style.display = 'block';
					} else if(lastname==""){
						$scope.message = "Merci de renseigner votre nom!";
						document.getElementById("message").style.display = 'block';
					} else if(mail==""){
						$scope.message = "Merci de renseigner votre e-mail!";
						document.getElementById("message").style.display = 'block';
					} else if(adresse==""){
						$scope.message = "Merci de renseigner votre adresse complète!";
						document.getElementById("message").style.display = 'block';
					} else if(ville==""){
						$scope.message = "Merci de renseigner votre adresse complète!";
						document.getElementById("message").style.display = 'block';
					} else if(pays==""){
						$scope.message = "Merci de renseigner votre adresse complète!";
						document.getElementById("message").style.display = 'block';
					} else if(codePostal==""){
						$scope.message = "Merci de renseigner votre adresse complète!";
						document.getElementById("message").style.display = 'block';
					} else if(psw==""){
						$scope.message = "Merci de renseigner un mot de passe!";
						document.getElementById("message").style.display = 'block';
					} else {
						
						if(!bonmail(mail)){ 
							$scope.message = "Veuiller entrez un mail valide!";
							document.getElementById("message").style.display = 'block';
						} else{
							$scope.message = "Compte créé!";

							$scope.user.name=name;
							$scope.user.lastname=lastname;
							$scope.user.mail=mail;
							$scope.user.address=adresse;
							$scope.user.city=ville;
							$scope.user.country=pays;
							$scope.user.postalcode=codePostal;
							$scope.user.password=psw;

							$http.post('/user', $scope.user).success(function(response) {
								alert("Vous avez bien été inscrit");
								window.location.assign('../');
							});
						}
					}
				} else {
					$scope.message = "L'Email que vous avez rensigner est déjà rattaché à un compte!";
					document.getElementById("message").style.display = 'block';
				}
			});
		} else{
			$scope.message = "Merci de renseigner tout les champs!";
			document.getElementById("message").style.display = 'block';
		}
	}

}]);﻿
