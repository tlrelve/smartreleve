/**
 * @author claireDupont
 */
 (function () {
  'use strict';

  angular.module('SmartReleve.app',[])
  .controller('appCtrl', appCtrl);

  /** @ngInject */
  function appCtrl($scope, $http) {
    $scope.handleSession = function (){
      if (window.sessionStorage.length<=0){
        console.log("SESSION VIDE");
        window.location.assign('/');
      }
    }

    $scope.handleSession();

    $scope.deconnection = function(){
      window.sessionStorage.removeItem('id');
      window.sessionStorage.removeItem('nom');
      window.sessionStorage.removeItem('prenom');
      window.sessionStorage.removeItem('mail');
    }
  }

})();
