/**
 * Created by INNEASOFT on 07/02/2017.
 */
var express = require('express');
var router = express.Router();

var fs = require('fs');
var _ = require('lodash');
var packets = [];

var User = require('./db').User;
var Frame = require('./db').Frame;
var Consom = require('./db').Consom;


const CODE = {
    PULSE_COUNTER_INDEX: "Compteur index",
    DEVICE_CONFIG: " Configuration",
    PULSE_COUNTER_CONFIG_1: "",
    PULSE_COUNTER_CONFIG_2: "",
};

const STATUS = {
    NO_ERROR: 'No Error',
    LOW_BAT_ERROR: 'LOW BATT',
    CONFIG_SWITCH_ERROR: 'CONFIG SWITCH',
    HW_ERROR: 'HW ERROR',
    CONFIG_DONE: 'CONFIG DOWNLINK'
};

const TYPE_PULSE = {
    NO_PULSE: '0',
    PULSE_AUTO: '3',
    GAZ: '4',
    ELECTRIC: '5',
    WATER_1: '6',
    WATER_2: '7',
    WATER_3: '8',
    ENERGY_THERMIC: '9'
};

// Loading Data 
loadData('packet-net.json');

// Routing pages
router.get('/', function (req, res) {
    var buffer = '';
    packets.forEach(function (packet) {
        buffer += '<a href="/decoding/' + packet.device_id + '">' + packet.device_id + '</a><br>';
    });
    //res.send(JSON.stringify(packets, null, 2));
    res.render('consommation', {packets: packets});
    //res.send(buffer);
});

function decode(data, paquet) {
    var buffer = new Buffer(data, 'binary');
    buffer.write(data);
    var code = parseInt(buffer.slice(0, 2), 16).toString(10);
    switch (code) {
        case '2':
            code = CODE.PULSE_COUNTER_INDEX;
            break;
        case '3':
            code = CODE.DEVICE_CONFIG;
            break;
        case '4':
            code = CODE.PULSE_COUNTER_CONFIG_1;
            break;
        case '5':
            code = CODE.PULSE_COUNTER_CONFIG_2;
            break;
        default:
            code = "";
    }
    var status = parseInt(buffer.slice(2, 4), 16).toString(2); // c0 :  11000000 ==>  1100 : N° trame ; 0000 : Etat trame (HW : bit3 , Switch : bit2 , Low Bat : bit1 , config : bit0 )

    switch (parseInt(status.substr(4, 8), 2).toString(10)) {
        case '1':
            status = STATUS.CONFIG_DONE;
            break;
        case '8':
            status = STATUS.HW_ERROR;
            break;
        case '4':
            status = STATUS.CONFIG_SWITCH_ERROR;
            break;
        case '2':
            status = STATUS.LOW_BAT_ERROR;
            break;
        case '0':
            status = STATUS.NO_ERROR;
            break;
        default:
            "";
    }
    var typePulse = parseInt(buffer.slice(4, 6), 16).toString(2);
    switch (parseInt(typePulse.substr(4, 8), 2).toString(10)) {
        case '1':
            typePulse = TYPE_PULSE.NO_PULSE;
            break;
        case '8':
            typePulse = TYPE_PULSE.GAZ;
            break;
        case '4':
            typePulse = TYPE_PULSE.ELECTRIC;
            break;
        case '2':
            typePulse = TYPE_PULSE.WATER_1;
            break;
        case '0':
            typePulse = TYPE_PULSE.ENERGY_THERMIC;
            break;
        default:
            "";
    }
    var index1 = buffer.slice(6, 8);
    var index2 = buffer.slice(8, 10);
    var index3 = buffer.slice(10, 12);
    var index4 = buffer.slice(12, 14);
    var valeurIndex = parseInt(index4 + index3 + index2 + index1, 16).toString(10) / 10;

    var index11 = buffer.slice(16, 18);
    var index12 = buffer.slice(18, 20);
    var index13 = buffer.slice(20, 22);
    var index14 = buffer.slice(22, 24);
    var valeurIndex2 = parseInt(index14 + index13 + index12 + index11, 16).toString(10) / 10;

    paquet.code = code;
    paquet.status = status;
    paquet.valeurIndex1 = valeurIndex;
    paquet.valeurIndex2 = valeurIndex2;

};


function loadData(filename) {
    fs.watch(filename, function (event, filename) {
        console.log('new data recieved: ' + event);
        if (filename) {
            fs.readFile(filename, {encoding: 'utf-8'}, function (err, data) {
                if (err) throw err
                JSON.parse(data).forEach(function (packet) {
                    decode(packet.data, packet);
                    var frame = new Frame(packet);
                    frame.save();
                    var consom = new Consom({
                        numeroCompteur: packet.device_id,
                        value: packet.valeurIndex1,
                        valueDate: packet.timestamp
                    });
                    consom.save();
                    packets.push(packet);
                });
            });
        } else {
            console.log('error data');
        }
    });

}

module.exports = router;



