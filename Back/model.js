//lets require/import the mongodb native drivers.
var mongodb = require('mongodb');

//We need to work with "MongoClient" interface in order to connect to a mongodb server.
var MongoClient = mongodb.MongoClient;

// Connection URL. This is where your mongodb server is running.
var url = 'mongodb://localhost:27017/tele';

// Use connect method to connect to the Server
MongoClient.connect(url, function (err, db) {
  if (err) {
    console.log('Unable to connect to the mongoDB server. Error:', err);
  } else {
    //HURRAY!! We are connected. :)
    console.log('Connection established to', url);

    userCol(db,function(){})
  }
});

var userCol = function(db,callback) {
    // Get the documents collection
    var collection = db.collection('users');
    //Create some users
    var user1 = {name: 'Mauro', lastname: 'Diaz', login: 'Mauro', password: 'mauro123', mail: 'dm.diaz14@uniandes.edu.co', address: 'allee berlioz 361', potalCode: 38400, city: 'Grenoble', country: 'France', idCounter: 'DSFSD6886768SD', idContract: 'HUIP8098'};

    // Insert some users
    collection.insert([user1], function (err, result) {
      if (err) {
        console.log(err);
      } else {
        console.log('Inserted %d documents into the "users" collection. The documents inserted with "_id" are:', result.length, result);
      }
      //Close connection
      contractCol(db,function(){})
    });
};

var contractCol = function(db,callback) {
    // Get the documents collection
    var collection = db.collection('contract');
    //Create some users
    var contract1 = {idContract: 'HUIP8098', dateDebu: new Date(2013,11,10,2,35), dateFin: new Date(2015,11,10,2,35)};

    // Insert some users
    collection.insert([contract1], function (err, result) {
      if (err) {
        console.log(err);
      } else {
        console.log('Inserted %d documents into the "users" collection. The documents inserted with "_id" are:', result.length, result);
      }
      //Close connection
      counterCol(db,function(){})
    });
};

var counterCol = function(db,callback) {
    // Get the documents collection
    var collection = db.collection('counter');
    //Create some users
    var counter1 = {idCounter: 'DSFSD6886768SD', type: 'L´EAU' , registretionDate: new Date(2015,11,10,2,35)};

    // Insert some users
    collection.insert([counter1], function (err, result) {
      if (err) {
        console.log(err);
      } else {
        console.log('Inserted %d documents into the "users" collection. The documents inserted with "_id" are:', result.length, result);
      }
      //Close connection
       consumptionCol(db,function(){})
    });
};

var consumptionCol = function(db,callback) {
    // Get the documents collection
    var collection = db.collection('consumption');
    //Create some users
    var consumption1 = {idConsumption: 'DSFSD68856867GFFGG', idCounter: 'DSFSD6886768SD', value: 21343, status: 'active', valuesDate: new Date(2015,12,10,2,35)};

    // Insert some users
    collection.insert([consumption1], function (err, result) {
      if (err) {
        console.log(err);
      } else {
        console.log('Inserted %d documents into the "users" collection. The documents inserted with "_id" are:', result.length, result);
      }
      //Close connection
      db.close();
    });
};
