var uri = "mongodb://localhost:27017/tele";

var mongoose = require('mongoose');
mongoose.connect(uri);

var db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error :'))
db.once('open', function (callback) {
    console.log('connection to database');
});

var Schema = mongoose.Schema;

var userSchema = new Schema({
    name: String,
    lastname: String,
    mail: String,
    postalCode: Number,
    city: String,
    country: String,
    password: String
}, {collection: 'user'});

exports.User = mongoose.model('User', userSchema);

var frameSchema = new Schema({
    device_id: String,
    timestamp: Date,
    data: String
}, {collection: 'trame'});

exports.Frame = mongoose.model('Frame', frameSchema);

var consomSchema = new Schema({
    numeroCompteur: String,
    valueDate: Date,
    value: Number
}, {collection: 'consommation'});

exports.Consom = mongoose.model('Consom', consomSchema);