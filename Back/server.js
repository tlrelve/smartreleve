//requires
var assert = require('assert'),
 express = require('express'),
 mongojs = require('mongojs'),
 bodyParser = require('body-parser'),
 fs = require('fs');

//Variables globales
var app = express();
var db = mongojs('tele', ['user','counter','comsuption','contract','contactlist']);

app.use(express.static(__dirname + '/public'));
app.use('/bower_components',express.static(__dirname + '/bower_components'));
app.use(bodyParser.json());

//On vide la BD
db.user.drop();
db.counter.drop();
db.comsuption.drop();
db.contract.drop();
db.contactlist.drop();

// Insertion des données en BD
fs.readFile(__dirname + '/public/json/user.json', 'utf8', function (err, data) {
  if (err) throw err;
  var json = JSON.parse(data);

  db.user.insert(json, function(err, doc) {
    if(err) throw err;
  });
});

fs.readFile(__dirname + '/public/json/compteur.json', 'utf8', function (err, data) {
  if (err) throw err;
  var json = JSON.parse(data);

  db.counter.insert(json, function(err, doc) {
  if(err) throw err;
  });
});

fs.readFile(__dirname + '/public/json/consommation.json', 'utf8', function (err, data) {
  if (err) throw err;
  var json = JSON.parse(data);

  db.comsuption.insert(json, function(err, doc) {
  if(err) throw err;
  });
});


app.get('/user', function (req, res) {
  db.user.find(function (err, docs) {
    res.json(docs);
  });
});

app.post('/user', function (req, res) {
  db.user.insert(req.body, function(err, doc) {
    res.json(doc);
  });
});

app.delete('/user/:id', function (req, res) {
  var id = req.params.id;
  db.user.remove({_id: mongojs.ObjectId(id)}, function (err, doc) {
    res.json(doc);
  });
});

app.get('/user/:id', function (req, res) {
  var id = req.params.id;
  db.user.findOne({_id: mongojs.ObjectId(id)}, function (err, doc) {
    res.json(doc);
  });
});

app.get('/user/mail/:mail', function (req, res) {
  var mail = req.params.mail;
  db.user.find({mail: mail}, function (err, doc) {
    res.json(doc);
  });
});

app.put('/user/:id', function (req, res) {
  var id = req.params.id;
  db.user.findOne({_id: mongojs.ObjectId(id)}, function (err, doc) {
    if (doc.mail !== req.body.mail)
    {
      db.counter.update(
        {email_user : doc.mail},
        {$set:{email_user : req.body.mail}},
        {multi : true}, function (err2, doc2) {
          console.log("compteur modifié de : "+ doc.mail +" a "+req.body.mail);
          console.log(doc2);
        });
    }
  });

  if(req.body != null && req.body.password != null && req.body.password != ""){
    db.user.findAndModify({
      query: {_id: mongojs.ObjectId(id)},
      update: {$set: {name: req.body.name, lastname: req.body.lastname, password: req.body.password, mail: req.body.mail, address: req.body.address, postalcode: req.body.postalcode, city: req.body.city, country: req.body.country, role: req.body.role}},
      new: true}, function (err, doc) {
        res.json(doc);
      }
    );
  } else {
    db.user.findAndModify({
      query: {_id: mongojs.ObjectId(id)},
      update: {$set: {name: req.body.name, lastname: req.body.lastname, mail: req.body.mail, address: req.body.address, postalcode: req.body.postalcode, city: req.body.city, country: req.body.country, role: req.body.role}},
      new: true}, function (err, doc) {
        res.json(doc);
      }
    );
  }
});

app.get('/counterlist', function (req, res) {
     db.counter.distinct('numeroCompteur',{},function (err, docs) {
     res.json(docs);
   });
});

app.get('/counter', function (req, res) {
  db.counter.find(function (err, docs) {
    res.json(docs);
  });
});

app.post('/counter', function (req, res) {
  db.counter.insert(req.body, function(err, doc) {
    res.json(doc);
  });
});

//Suppression d'un compteur
app.delete('/counter/:id', function (req, res) {
  var id = req.params.id;
  db.counter.remove({_id: mongojs.ObjectId(id)}, function (err, doc) {
    res.json(doc);
  });
});

app.get('/counter/:id', function (req, res) {
  var id = req.params.id;
  db.counter.findOne({_id: mongojs.ObjectId(id)}, function (err, doc) {
    res.json(doc);
  });
});

//Modification d'un compteur
app.put('/counter/:id', function (req, res) {
  var id = req.params.id;
  db.counter.findAndModify({
      query: {_id: mongojs.ObjectId(id)},
      update: {
        $set: {
          nom: req.body.nom,
          numeroCompteur: req.body.numero,
          modele: req.body.modele,
          type: req.body.type,
          registretionDate: req.body.date,
          email_user: req.body.mail
        }
      },
      new: true,
      upsert: true
    },
    function (err, doc) {
      res.json(doc);
    }
  );
});

//Insertion d'un compteur
app.put('/counter/new/:mail', function (req, res) {
  var mail = req.params.mail;
  db.counter.insert({
      nom: req.body.nom,
      numeroCompteur: req.body.numero,
      modele: req.body.modele,
      type: req.body.type,
      registretionDate: req.body.date,
      email_user: mail
    },
    function (err, doc) {
      res.json(doc);
    }
  );
});

app.get('/counter/mail/:mail', function (req, res) {
  var mail = req.params.mail;
  db.counter.find({email_user: mail}, function (err, doc) {
    res.json(doc);
  });
});

app.get('/userCounter/:userMail', function (req, res) {
  var userMail = req.params.userMail;
  db.counter.find({email_user: userMail},{'numeroCompteur':true}, function (err, doc) {
    res.json(doc);
  });
});

app.get('/userConsommation/:numCompteur', function (req, res) {
  var numCompteur = req.params.numCompteur;
  db.comsuption.find({numeroCompteur: numCompteur}, function (err, doc) {
    res.json(doc);
  });
});

app.get('/comsuption', function (req, res) {
  db.comsuption.find(function (err, docs) {
    res.json(docs);
  });
});

app.post('/comsuption', function (req, res) {
  db.comsuption.insert(req.body, function(err, doc) {
    res.json(doc);
  });
});

app.delete('/comsuption/:id', function (req, res) {
  var id = req.params.id;
  db.comsuption.remove({_id: mongojs.ObjectId(id)}, function (err, doc) {
    res.json(doc);
  });
});

app.get('/comsuption/:id', function (req, res) {
  var id = req.params.id;
  db.comsuption.findOne({_id: mongojs.ObjectId(id)}, function (err, doc) {
    res.json(doc);
  });
});

app.put('/comsuption/:id', function (req, res) {
  var id = req.params.id;
  db.comsuption.findAndModify({
    query: {_id: mongojs.ObjectId(id)},
    update: {$set: {numeroCompteur: req.body.numeroCompteur, value: req.body.value, valueDate: req.body.valueDate}},
    new: true}, function (err, doc) {
      res.json(doc);
    }
  );
});

app.get('/contract', function (req, res) {
  db.contract.find(function (err, docs) {
    res.json(docs);
  });
});

app.post('/contract', function (req, res) {
  db.contract.insert(req.body, function(err, doc) {
    res.json(doc);
  });
});

app.delete('/contract/:id', function (req, res) {
  var id = req.params.id;
  db.contract.remove({_id: mongojs.ObjectId(id)}, function (err, doc) {
    res.json(doc);
  });
});

app.get('/contract/:id', function (req, res) {
  var id = req.params.id;
  db.contract.findOne({_id: mongojs.ObjectId(id)}, function (err, doc) {
    res.json(doc);
  });
});

app.put('/contract/:id', function (req, res) {
  var id = req.params.id;
  db.contract.findAndModify({
    query: {_id: mongojs.ObjectId(id)},
    update: {$set: {numeroCompteur: req.body.numeroCompteur, idUser: req.body.user_email, dateDebut: req.body.dateDebut, dateFin: req.body.dateFin}},
    new: true}, function (err, doc) {
      res.json(doc);
    }
  );
});

app.get('/contactlist', function (req, res) {
  db.contactlist.find(function (err, docs) {
    res.json(docs);
  });
});

app.get('/user/:mail/:password', function(req, res){
  var email = req.params.mail;
  var pswd = req.params.password;
  db.user.find({mail: email, password: pswd}, function(err, doc){
    res.json(doc);
  });
});

app.post('/contactlist', function (req, res) {
  db.contactlist.insert(req.body, function(err, doc) {
    res.json(doc);
  });
});

app.delete('/contactlist/:id', function (req, res) {
  var id = req.params.id;
  db.contactlist.remove({_id: mongojs.ObjectId(id)}, function (err, doc) {
    res.json(doc);
  });
});

app.get('/contactlist/:id', function (req, res) {
  var id = req.params.id;
  db.contactlist.findOne({_id: mongojs.ObjectId(id)}, function (err, doc) {
    res.json(doc);
  });
});

app.put('/contactlist/:id', function (req, res) {
  var id = req.params.id;
  db.contactlist.findAndModify({
    query: {_id: mongojs.ObjectId(id)},
    update: {$set: {name: req.body.name, email: req.body.email, number: req.body.number}},
    new: true}, function (err, doc) {
      res.json(doc);
    }
  );
});

app.listen(3000);
console.log("Server running on port 3000");
