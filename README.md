# Guide d'installation #

## Environnement ##
###NodeJs###
Télécharger nodeJs : [https://nodejs.org/en/](https://nodejs.org/en/)

###Bower###
Ouvrir l’invite de commande
Taper la commande : **npm install -g bower**

### Mongo ###
Télécharger MongoDB : [https://www.mongodb.com/download-center#community](https://www.mongodb.com/download-center#community)
Choisir “Windows Server 2008 R2 64-bit and later, with SSL support x64”.

### Navigateur web ###
Ne fonctionne pas sur internet exploreur.

## Lancement ##
### Télécharger le projet ###
Télécharger le projet : [https://bitbucket.org/tlrelve/smartreleve](https://bitbucket.org/tlrelve/smartreleve)

### Installer les modules ###
Ouvrir l’invite de commande. 

Aller dans le dossier smartreleve/Back/
Taper la commande : **npm install**

### Lancer un serveur local ###
Ouvrir l’invite de commande.
Se rendre dans le répertoire d’installation de MongoDB puis dans /Server/3.4/bin/ et executer la commande: **mongod**

Ouvrir un autre invite de commandes et se rendre dans le dossier d’installation de SmartRelève puis dans smartreleve/Back/, et executer la commande : **node server.js**

**Le serveur est lancé, l’application tourne.**

Ouvrir un navigateur et se rendre sur l’URL [localhost:3000](localhost:3000)





# Tutoriel git #
## Les commandes ##
Pour savoir où tu en es (quelle branche, quel fichier modifié, quel fichier commité,...) :

* **git status**
⇒  en rouge tes fichiers modifiés

⇒  en vert tes fichiers ajoutés


Pour ajouter des fichiers ou répertoires

* **git add** <NOM_FICHIER>

Pour ajouter d’un coup tous les fichiers modifiés

* **git add .**

Pour commiter des fichiers

* **git commit -m “**<MESSAGE>**”**
⇒  les fichiers vert du **git status** seront commités

À CHAQUE COMMIT : indiquer dans le message sur quelle fonctionnalité vous travaillez, et dites succinctement ce que vous avez fait. (Exemple : <Tache X>: ajout fonction Y)


Pour voir les différences avec le serveur

* **git fetch**

Pour récupérer les changements du serveur

Si pas de changement de ton côté (pas de commit entre temps)

* **git pull**
Si changement de ton côté

* **git rebase**

Pour pousser tes commits sur le serveur

* **git push**

Pour changer de branche

* **git checkout** <nom_branche>

Pour créer une branche

* **git checkout -b** <nom_branche>

Pour pousser une branche locale sur le serveur (la première fois)

* **git push origin** <nom_branche>

## Exemples ##
En général tu fait :


> git status

Sur la branche <taBranche>

Les fichiers modifiés en rouge

> git add .

> git status

Sur la branche <taBranche>

Les fichiers ajoutés en vert

> git commit -m “<MESSAGE>”

> git status

Sur la branche <taBranche>

rien à valider, la copie de travail est propre

> git fetch

> git status

Sur la branche <taBranche>, en retard de 3 commit

Faites un git pull

> git pull OU rebase

> git status

Sur la branche <taBranche>, en avance d’un commit

> git push

> git status 

Sur la branche <taBranche>

rien à valider, la copie de travail est propre


Pour merger ta branche avec master :


Ta branche est propre, tous les changements sont commités voire pushés

> git status 

Sur la branche <taBranche>

rien à valider, la copie de travail est propre

> git checkout master

> git fetch

> git pull

> git checkout <nom_branche>

> git merge master

> git status

Sur la branche <taBranche>

Les fichiers modifiés en rouge

Les fichiers ajoutés en vert

Résoudre les fichiers rouges qui ont des conflits, vérifier que ca marche toujours

> git add .

> git status

Sur la branche <taBranche>

Les fichiers ajoutés en vert

> git commit -m “<MESSAGE>”

> git push

> git checkout master

> git fetch


Si il y a eu des changements recommencer le merge sur la branche


> git merge <nom_branche>

> git push